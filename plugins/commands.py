#!/usr/bin/env python
import json
import logging
import random
import re
from random import randint
from typing import List

import requests
import wikitextparser as wtp
from pyrogram import filters
from pyrogram.client import Client
from simpleeval import simple_eval
from wikitextparser import remove_markup

logging.basicConfig(filename='log.txt', level=logging.WARNING)
log = logging.getLogger(__name__)

#@Client.on_raw_update()
#def parse_raw_update(client, update, users, chats, message):
	#if type(update).__name__ == "UpdateMessageReactions":
		#last_emoji = update.reactions.results[-1].reaction.emoticon
		#if hasattr(update.peer, 'channel_id'):
			#peer_id = int("-100" + str(update.peer.channel_id))
		#elif hasattr(update.peer, 'chat_id'):
			#peer_id = int("-" + str(update.peer.chat_id))
		#else:
			#peer_id = update.peer.user_id
		#message_id = update.msg_id
		#message.text_reply("message_id: " + str(message_id))

@Client.on_message(filters.command("nome", "!"))
def nomino(client, message):
	diocristo = ['A-L', 'M-Z']
	diocanoide = random.choice(tuple(diocristo))
	url = "https://it.wikipedia.org/w/index.php?title=Prenomi_italiani_(" + diocanoide + ")&action=raw"
	r = requests.get(url)
	wt = wtp.parse(r.text)
	rand_tab = random.choice(wt.tables).data()
	rand_row = list(filter(None, random.choice(rand_tab)))
	# tabella
	rand_tab = random.choice(wt.tables).data()
	# riga
	rand_row = list(filter(None, random.choice(rand_tab)))
	# colonna
	rand_nam = re.sub(".*\\||\\]\\]", "", re.sub("\\[\\[#.*", "", re.sub("^\\[\\[", "", random.choice(rand_row))))
	message.reply_text("<code>" + remove_markup(rand_nam, replace_wikilinks=False) + "</code>")


scarti = [
	'-1boy', '-2boys', '-multiple_boys',
	'-fusion', '-colored_sclera','-hetero',
	'-monster','-3d,', '-cgi',
	'-size_difference','-giant', '-giantess',
	'-inflation', '-weight_gain', '-slightly_chubby',
	'-gore', '-scat', '-male', '-pregnant', '-inflation',
	'-fat','-diaper', '-bald', '-gigantic_breasts', '-huge_nipples',
	'-long_nipples'
]
scartia = [
	'-faceless_male','-ugly_man', '-ugly_bastard', '-body_writing',
	'-rape','-yaoi', '-multiple_balls', '-interspecies',
	'-interracial', '-dark_skinned_male',
	'-horse_cock','-animal_sex',
	'-peeing','-pee', '-pee_stain',
	'-child', '-baby', '-tentacle_sex', '-parasite',
	'-tentacles', "-sonic_the_hedgehog", '-miles_prower', 
	'-lewdwigvondrake', '-knuckles_the_echidna', '-bug',
	'-damian_desmond', '-beastiality', '-animal_penis', 
	'-knotted_penis', 
	]
no_video = ['-video', '-animation']

g_website = "https://gelbooru.com/index.php?"
g_key = "api_key=0aac5acdbfeba232e46aebfee95897cfdb96a10b444dff13f52cc90c83cebc49&user_id=967498&"
g_params = "page=dapi&s=post&json=1&q=index&limit=1&tags=sort:random+"
gelbooru_api = g_website + g_key + g_params

e_website = "https://e621.net/posts.json?"
e_params = "limit=1&tags=order:random+"
e621_api = e_website + e_params

consfw = True
tags: List[type[str]] = []
leniency = 0

def switchino(message):
	global consfw
	inter = " ".join(message.text.split()[1:])
	if inter == "on":
		message.reply_text("<code>Horny attivato</code>")
		consfw = True
	elif inter == "off":
		message.reply_text("<code>Horny disattivato</code>")
		consfw = False

def linkino(message, consfw, leniency):
	sesso = random.choices(population=['rating:general', 'rating:sensitive'], weights=succinto(leniency))
	msg = " ".join(message.text.split()[1:])
	horny = re.match("nsfw", msg)
	spl = re.match("spl", msg)
	if horny is not None and consfw == True:
		leniency = 0
		sesso = random.choices(population=['rating:questionable', 'rating:explicit'], weights=succinto(leniency))

	if spl is not None: spoiler = True
	else: spoiler = False
	return (sesso, spoiler)

def linkino2(message, consfw):
	sesso = ['rating:safe']
	msg = " ".join(message.text.split()[1:])
	horny = re.match("nsfw", msg)
	if horny is not None and consfw == True:
		leniency = 0
		sesso = random.choices(population=['rating:questionable', 'rating:explicit'], weights=succinto(leniency))
	return sesso

def pic(client, message, tags, leniency):
	(sesso, spoiler) = linkino(message, consfw, leniency)
	global query
	if sesso is None:
		query = no_video + tags
	else:
		query = no_video + tags + sesso
	req = json.loads(requests.get(gelbooru_api + '+'.join(query)).text)
	file_id = req['post'][0]['file_url']
	client.send_photo(
		chat_id=message.chat.id,
		photo=file_id,
		has_spoiler=spoiler)

def pic2(message, tags):
	sesso = linkino2(message, consfw)
	global query
	if sesso is None:
		query = no_video + tags
	else:
		query = no_video + tags + sesso
	url = e621_api + '+'.join(query)
	headers = {'user-agent' : 'olibot'}
	req = json.loads(requests.get(url, headers=headers).text)
	file_id = req['posts'][0]['file']['url']
	message.reply_photo(file_id)

def succinto(leniency):
	match leniency:
		case 2: leniency = [0.1, 0.9]
		case 1: leniency = [0.7, 0.3]
		case 0: leniency = [0.5, 0.5]
		case -1: leniency = [1.0, 0.0]
	return leniency

@Client.on_message(filters.command("test", "!"))
def test(client, message):
	message.reply_text("<code>icoli</code>", quote=True)


@Client.on_message(filters.command("mlaatr", "!"))
def mlaatr(client, message):
	message.reply_text("https://archive.org/details/teenage-robot-full-series")

set_img = """•oliwia (nsfw) (arg)\n•emily (nsfw) (arg) (alt emilia)\n•aurora (nsfw) (arg) (alt ro)\n•rosalinda (nsfw) (alt rosalina)
•jenny (nsfw)\n•anna (nsfw)\n•emma (arg)\n•eva (nsfw)\n•michiru (nsfw)\n•nazuna (nsfw)\n•michizuna (nsfw)\n•miao\n•femboy\n•sofia\n•basso
•kris\n•niko\n•ralsei\n•noelle\n•mizore\n•blessed\n•tails\n•amy\n•kooty\n•nickit\n•char (arg)\n"""
set_func = "•test\n•horny\n•status\n•mlaatr\n•sesso (str)\n•calc (expr)\n•cerca\n•ext\n•fix (broken)\n"
set_div = "•poppe\n•sto\n•no\n•misura\n•misure\n•nome\n•porco"

listaoli = 'ochako, random, jenny, tsuyu, futaba, daisy, blank, kumiko, michiru, gloria, vera, tsubasa, stella, pyra, noelle, kris, hajime, miyako, haruhi, isabelle, amy, yui, anya, mela, mirai, teto, nikki, velma, ankha, sage, 5volt, purah, yukari'
listaemi = 'miku, marnie, shygal, raven, nazuna, blaze, hutao, patchouli, rei, lucy, rebecca, muffet'
listaro = 'mizore, rosalinda, kikuri, rinko'
listaemma = 'kangel, frye, osaka'

@Client.on_message(filters.command("aiuto", "!"))
def aide(client, message):
	checkino = " ".join(message.text.split()[1:])
	bucchin = [
	'Comandi disponibili\n',
	"Mandano immagini (non aggiungere le parentesi)\n",
	'\nFunzionalità base\n', '\nStupide cose\n'
	]
	if checkino != "":
		match checkino:
			case 'oliwia': comandi = "Manda disegni a immagine e somiglianza di Oliwia\nArgomenti disponibili: " + listaoli
			case 'emily': comandi = "Manda disegni a immagine e somiglianza di Emily\nArgomenti disponibili: " + listaemi
			case 'emilia': comandi = "Manda disegni a immagine e somiglianza di Emily\nArgomenti disponibili: " + listaemi
			case 'aurora': comandi = "Manda disegni a immagine e somiglianza di Aurora\nArgomenti disponibili: " + listaro
			case 'anna': comandi = "Manda disegni a immagine e somiglianza di Anna"
			case 'emma': comandi = "Manda disegni a immagine e somiglianza di Emma\nArgomenti disponibili: " + listaemma
			case 'eva': comandi = "Manda disegni a immagine e somiglianza di Eva"
			case 'rosalinda': comandi = "Manda immagini con Rosalinda"
			case 'rosalina': comandi = "Manda immagini con Rosalinda"
			case 'jenny': comandi = "Manda immagini con Jenny"
			case 'miao': comandi = "Manda nekogirl"
			case 'femboy': comandi = "Manda femboy"
			case 'sofia': comandi = "Manda immagini feticiste"
			case 'basso': comandi = "Manda ragazze che hanno un basso"
			case 'kris': comandi = "Manda immagini con Kris"
			case 'noelle': comandi = "Manda immagini con Niko"
			case 'blessed': comandi = "(Almeno prova) Manda immagini wholesome"
			case 'ralsei': comandi = "Manda immagini con Ralsei"
			case 'noelle': comandi = "Manda immagini con Noelle"
			case 'mizore': comandi = "Manda immagini con Mizore"
			case 'amy': comandi = "Manda immagini con Amy"
			case 'kooty': comandi = "Manda immagini con Klonoa"
			case 'test': comandi = "Comando di test per vedere se il bot è attivo"
			case 'horny': comandi = "Attiva o disattiva il filtro per le immagini nsfw\nSolo Oliwia può usarlo"
			case 'status': comandi = "Verifica se il filtro è attivo o meno"
			case 'mlaatr': comandi = "Manda il link per guardare __My Life As A Teenage Robot__"
			case 'sesso': comandi = "Calcola il tuo valore di sessaggine\n!sesso -> Sei sesso al ...% I !sesso qualcosa -> Qualcosa è sesso al ...%\nPotrebbe dare risultati diversi con alcuni valori"
			case 'calc': comandi = "Calcolatrice basilare\n!calc 1+1 -> 2"
			case 'misura': comandi = "Calcola la lunghezza della tua minchia"
			case 'misure': comandi = "Calcola la grandezza delle tue zizze"
			case 'sto': comandi = "Risponde con cazzo"
			case 'no': comandi = "Risponde con una parola a caso aggiunta a no\n!no -> no bitches?"
			case 'nome': comandi = "Dà un nome proprio a caso"
			case 'porco': comandi = "Genera una bestemmia nel formato\nporco : nome : participio passato di un verbo (a volte non presente)"
			case 'cerca': comandi = "Cerca uno sticker pack"			
			case 'char': comandi = """Manda immagini dalla serie Little Tail Bronx\n!char x y\nx indica il gioco da cui viene ([s] Solatorobo, [t] Tail Concerto, [f] Fuga)\nPersonaggi disponibili:\ns (red, chocolat, elh, beluga, quebec, flo, bruno, opera, gren, calua, merveille, nero, blanck, carmine, rose)\nt (waffle, alicia, flare, stare, panta, russel, terria, cyan, fool)\nf (malt, mei, hanna, boron, kyle, socks, chick, hack, sheena, jin, wappa, britz, muscat, shvein, flam, doktor blutwurst)"""
			case 'ext': comandi = "Estrae l'embed da un link Pinterest"
			case 'fix': comandi = "Manda versione fixata di un link Twitter/Instagram"
			case _: comandi = "Non è un comando"
	else:
		comandi = bucchin[0] + "\n" + bucchin[1] + set_img + bucchin[2] + set_func + bucchin[3] + set_div
	message.reply_text("<code>" + comandi + "</code>")

@Client.on_message(filters.command("help", "!"))
def pomocy(client, message):
	checkino = " ".join(message.text.split()[1:])
	bucchin = ['Available commands\n', "Image senders (don't add parenthesis)\n",'\nBasic functions\n','\nStupid things\n']
	if checkino != "":
		match checkino:
			case 'oliwia': comandi = "Sends images based on Oliwia's likeness\nAvailable arguments: " + listaoli
			case 'emily': comandi = "Sends images based on Emily's likeness\nAvailable arguments: " + listaemi
			case 'emilia': comandi = "Sends images based on Emily's likeness\nAvailable arguments: " + listaemi
			case 'aurora': comandi = "Sends images based on Aurora's likeness\nAvailable arguments: " + listaro
			case 'anna': comandi = "Sends images based on Anna's likeness"
			case 'emma': comandi = "Sends images based on Emma's likeness\nAvailable arguments: " + listaemma
			case 'eva': comandi = "Sends images based on Eva's likeness"
			case 'rosalinda': comandi = "Sends images with Rosalina"
			case 'rosalina': comandi = "Sends images with Rosalina"
			case 'jenny': comandi = "Sends Jenny pics"
			case 'miao': comandi = "Sends nekogirls"
			case 'femboy': comandi = "Sends femboys"
			case 'sofia': comandi = "Sends foot fetish pics"
			case 'basso': comandi = "Sends bassist girls pics"
			case 'kris': comandi = "Sends Kris pics"
			case 'niko': comandi = "Sends Niko pics"
			case 'blessed': comandi = "Tries to send wholesome pics"
			case 'ralsei': comandi = "Sends Ralsei pics"
			case 'noelle': comandi = "Sends Noelle pics"
			case 'mizore': comandi = "Sends Mizore pics"
			case 'amy': comandi = "Sends Amy pics"
			case 'kooty': comandi = "Sends Klonoa pics"
			case 'test': comandi = "Test command to see if it works"
			case 'horny': comandi = "Turns explicit images on or off\nOnly Oliwia can use it"
			case 'status': comandi = "Checks whether the switch is turned on or off"
			case 'mlaatr': comandi = "Sends a link to watch My Life As A Teenage Robot"
			case 'sesso': comandi = "Gives your sexiness value, or that of a thing you added\n!sesso -> Sei sesso al ...% I !sesso qualcosa -> Qualcosa è sesso al ...%\nResult may vary depending on certain values"
			case 'calc': comandi = "Basic calculator\n!calc 1+1 -> 2"
			case 'misura': comandi = "Calculates your dick's size"
			case 'misure': comandi = "Calculates your tits' size"
			case 'sto': comandi = "Replies with cazzo"
			case 'no': comandi = "Replies with a random word after no\n!no -> no bitches?"
			case 'nome': comandi = "Gives a random given name"
			case 'porco': comandi = "Gives a random swear in this format\nporco : name : past participle of a verb (optional, sometimes won't show up)"
			case 'cerca': comandi = "Searches for a sticker pack"
			case 'char': comandi = """Sends pics from the Little Tail Bronx series\n!char x y\nx pointing out the game it comes from ([s] Solatorobo, [t] Tail Concerto, [f] Fuga)\nAvailable characters:\ns (red, chocolat, elh, beluga, quebec, flo, bruno, opera, gren, calua, merveille, nero, blanck, carmine, rose)\nt (waffle, alicia, flare, stare, panta, russel, terria, cyan, fool)\nf (malt, mei, hanna, boron, kyle, socks, chick, hack, sheena, jin, wappa, britz, muscat, shvein, flam, doktor blutwurst)"""
			case 'ext': comandi = "Extracts an embeddable pic from a Pinterest link"
			case 'fix': comandi = "Returns a fixed Twitter/Instagram link"
			case _: comandi = "This is not a command"
	else:
		comandi = bucchin[0] + "\n" + bucchin[1] + set_img + bucchin[2] + set_func + bucchin[3] + set_div
	message.reply_text("<code>" + comandi + "</code>")

@Client.on_message(filters.command("sesso", "!"))
def sessometro(client, message):
	nome = " ".join(message.text.split()[1:])
	ultrasesso = "emily|fabiola|emi|neek|sofia|yuri|fem[A-Za-z]$|donn.$|trans|mtf|jenny|rosalinda|mizore|aurora|ro'|nozomi|nicole|arianna|kooty|kootino|ilma|vittoria|florysta|mamacita"
	antisesso = "terf|omofob|transfob|yaoi|gore|femminicidio"
	if nome != "":
		match = re.search(ultrasesso, str(message.text), flags=re.IGNORECASE)
		match2 = re.search(antisesso, str(message.text), flags=re.IGNORECASE)
		napoli = re.search("oliwia|oliwieta", str(message.text), flags=re.IGNORECASE)
		if (match2 is not None or (match is not None and match2 is not None)):
			sessaggine = "Fa assolutamente schifo"
		elif match is not None and match2 is None:
			sessaggine = nome + " è ultrasesso"
		elif nome == "piedini":
			sessaggine = "sofia smettila"
		elif napoli is not None:
			sessaggine = nome + " è sesso di Napoli livello"
		else:
			sessaggine = nome + " è sesso al " + str(randint(0, 100)) + "%"
	else:
		sessaggine = "Sei sesso al " + str(randint(0, 100)) + "%"
	message.reply_text("<code>" + sessaggine + "</code>")

@Client.on_message(filters.command("jenny", "!"))
def jenny(client, message):
	tags = ['jenny_wakeman', '-foot_focus', '-alternate_color', '-dark_skin', '-foot_fetish'
	'-fernando_faria', '-slb', '-racaseal', '-haydee', '-dc_comics', '-huge_breasts', '-huge_thighs',
	'-thick_thighs', '-big_breasts'] + scarti
	pic2(message, tags)
	
@Client.on_message(filters.command("oliwia", "!"))
def oliwia(client, message):
	leniency = 1
	meg = " ".join(message.text.split()[1:])
	match_set = listaoli.split(", ")
	tag_set = [
		"uraraka_ochako+-midoriya_izuku", "1girl+brown_hair+glasses+short_hair",
		"jenny_wakeman", "asui_tsuyu+-midoriya_izuku", "sakura_futaba",
		"princess_daisy", "blank_(captainkirb)", "oumae_kumiko",
		"kagemori_michiru", "gloria_(pokemon)", "may_(pokemon)",
		"hanekawa_tsubasa", "starfire", "pyra_(xenoblade)",
		"noelle_holiday", "kris_(deltarune)", "owari_hajime",
		"hoshino_miyako_(wataten)", "suzumiya_haruhi", "isabelle_(animal_crossing)",
		"amy_rose", "hirasawa_yui", "anya_(spy_x_family)+rating:general", "mela_(pokemon)",
		"kuriyama_mirai", "kasane_teto", "nikki_(swapnote)", "velma_dace_dinkley",
		"ankha_(animal_crossing)", "sage_(sonic)", "5-volt", "purah", "akiyama_yukari"
	]
	if meg != "":
		for x in range(len(match_set)):
			match = re.search(match_set[x], meg, flags=re.IGNORECASE)
			if match is not None:
				if match != match_set[6]:
					tags =  [tag_set[x]] + scarti + scartia
				else:
					tags = [tag_set[x]]
				break
			else:
				tags = [random.choice(tuple(tag_set))] + scarti + scartia
			pic(client, message, tags, leniency)
			break
	else:
		tags = [random.choice(tuple(tag_set))] + scarti + scartia
		pic(client, message, tags, leniency)

@Client.on_message(filters.command("emily|emilia", "!"))
def emily(client, message):
	leniency = 1
	meg = " ".join(message.text.split()[1:])
	match_set = listaemi.split(", ")
	tag_set = [
		"hatsune_miku", "marnie_(pokemon)", "shy_gal", "raven_(dc)",
		"hiwatashi_nazuna", "blaze_the_cat", "hu_tao_(genshin_impact)",
		"patchouli_knowledge", "ayanami_rei", "lucy_(cyberpunk)",
		"rebecca_(cyberpunk)", "muffet"
	]
	if meg != "":
		for x in range(len(match_set)):
			match = re.search(match_set[x], meg, flags=re.IGNORECASE)
			if match is not None:
				tags = [tag_set[x]] + scarti + scartia
				break
			else:
				tags = [random.choice(tag_set)] + scarti + scartia
			pic(client, message, tags, leniency)
			break
	else:
		tags = [random.choice(tag_set)] + scarti + scartia
		pic(client, message, tags, leniency)

@Client.on_message(filters.command("anna", "!"))
def annie(client, message):
	leniency = 2
	annina = ['marceline_abadeer', 'futanari']
	tags = [random.choice(annina)] + scarti + scartia
	pic(client, message, tags, leniency)

@Client.on_message(filters.command("aurora|ro", "!"))
def aurorina(client, message):
	leniency = 1
	meg = " ".join(message.text.split()[1:])
	match_set = listaro.split(", ")
	tag_set = ['yoroizuka_mizore', 'rosalina', 'hiroi_kikuri', 'shirokane_rinko']
	if meg != "":
		for x in range(len(match_set)):
			match = re.search(match_set[x], meg, flags=re.IGNORECASE)
			if match is not None:
				tags = [tag_set[x]]
				break
			else:
				tags = [random.choice(tag_set)]
			pic(client, message, tags, leniency)
			break
	else:
		tags = [random.choice(tag_set)]
		if tags != tag_set[0]:
			tags = tags + scarti + scartia
			pic(client, message, tags, leniency)

@Client.on_message(filters.command("emma", "!"))
def emmina(client, message):
	leniency = -1
	meg = " ".join(message.text.split()[1:])
	match_set = listaemma.split(", ")
	tag_set = ['needy_girl_overdose', 'frye_(splatoon)', 'kasuga_ayumu']
	if meg != "":
		for x in range(len(match_set)):
			match = re.search(match_set[x], meg, flags=re.IGNORECASE)
			if match is not None:
				tags = [tag_set[x]]
				break
			else:
				tags = [random.choice(tag_set)]
	else:
		tags = [random.choice(tag_set)]
		if tags != tag_set[0]:
			tags = tags + scarti + scartia
			pic(client, message, tags, leniency)	

@Client.on_message(filters.command("eva", "!"))
def eva(client, message):
	leniency = 1
	tags = ['katsuragi_misato'] + scarti + scartia
	pic(client, message, tags, leniency)

@Client.on_message(filters.command("noi", "!"))
def noi(client, message):
	leniency = 1
	tags = ['3girls'] + scarti + scartia
	pic(client, message, tags, leniency)

@Client.on_message(filters.command("neek", "!"))
def neek(client, message):
	leniency = 1
	tags = ['*long_hair', 'black_hair', '-pussy', '-bangs', '-child', '-loli', 'flat_chest', 'solo']
	pic(client, message, tags, leniency)

@Client.on_message(filters.command("miao", "!"))
def miao(client, message):
	leniency = 0
	tags = ['cat_girl'] + scarti + scartia
	pic(client, message, tags, leniency)

@Client.on_message(filters.command("mizore|mziore|mizorw|mizrow|mizroe|mziroe|mizoee|mixore|mizoew|mziuore|mziorew|mzireo|mzioire|miozre|mizoer", "!"))
def mamma(client, message):
	leniency = 0
	tags = ['yoroizuka_mizore']
	pic(client, message, tags, leniency)

@Client.on_message(filters.command("rosalinda|rosalina", "!"))
def rosalinda(client, message):
	leniency = 0
	tags = ['rosalina', '-furry', '-super_smash_bros.', '-bowser', '-animal_crossing', '-mario'] + scarti + scartia
	pic(client, message, tags, leniency)

@Client.on_message(filters.command("sofia", "!"))
def sofia(client, message):
	leniency = 0
	tags = ['1girl', 'foot_focus'] + scarti + scartia
	pic(client, message, tags, leniency)

@Client.on_message(filters.command("michiru", "!"))
def michiru(client, message):
	base = ['michiru_kagemori'] 
	tags = scarti + scartia
	remover(len(tags), len(tags)-1, tags, 36)
	tags.extend(base)
	pic2(message, tags)

@Client.on_message(filters.command("nazuna", "!"))
def nazuna(client, message):
	base = ['nazuna_hiwatashi']
	tags = scarti + scartia
	remover(len(tags), len(tags)-1, tags, 36)
	tags.extend(base)
	pic2(message, tags)

@Client.on_message(filters.command("michizuna", "!"))
def michi(client, message):
	base = ['nazuna_hiwatashi', 'michiru_kagemori']
	tags = scarti + scartia
	remover(len(tags), len(tags)-1, tags, 35)
	tags.extend(base)
	pic2(message, tags)

@Client.on_message(filters.command("bna", "!"))
def bna(client, message):
	ah = [True, False]
	eh = random.choice(ah)
	if eh: michiru(client, message)
	else: nazuna(client, message)

@Client.on_message(filters.command("ralsei", "!"))
def ralsei(client, message):
	tags = ['ralsei', 'rating:safe'] + scartia
	pic2(message, tags)

@Client.on_message(filters.command("kooty", "!"))
def kooty(client, message):
	tags = ['klonoa', 'rating:safe', '-foot_focus', '-diaper', '-savestate'] + scartia
	pic2(message, tags)

@Client.on_message(filters.command("nickit", "!"))
def nickit(client, message):
	tags = ['nickit', 'rating:safe', '-foot_focus', '-diaper'] + scartia
	pic2(message, tags)

@Client.on_message(filters.command("noelle", "!"))
def noelle(client, message):
	tags = ['noelle_holiday', 'rating:safe'] + scartia
	pic2(message, tags)

@Client.on_message(filters.command("niko", "!"))
def miaomiao(client, message):
	leniency = -1
	tags = ['niko_(oneshot)']
	pic(client, message, tags, leniency)

#tag remover

def remover(limit, cosa, tags, lv):
	for x in range(limit-lv): #rimuove cose per rientrare nel limite
		tags.pop(random.randint(0, cosa-1))
		cosa-=1
	return tags

@Client.on_message(filters.command("amy", "!"))
def cutie(client, message):
	base = ['amy_rose', 'rating:safe']
	tags = ['-voluptuous', '-thick_thighs', '-big_butt', '-butt', '-curvy_figure', 
	'-breasts', '-bikini', '-feet', '-skimpy', '-kissing', 
	'-possession', '-diaper', '-canid', '-mouse', '-higgyy', 
	'-anthrofied', '-transformation', '-mykegreywolf', '-cusith-baby', 
	'-ugandan_knuckles', '-1boy', '-2boys', '-multiple_boys','-fusion', 
	'-colored_sclera','-hetero','-monster','-3d,', '-cgi','-size_difference',
	'-weight_gain', '-slightly_chubby', '-male', '-alternate_species',
	'-jigglypuff', '-cub', '-amouge', '-pubs_(artist)', '-clothing_swap',
	'-seductive', '-darkfang100', '-floof-tigress', '-misaginus', '-humanization',
	'-nightmare_fuel']
	cosa = len(tags)
	if cosa>=36:
		limit = cosa
		remover(limit, cosa, tags, 36)
	tags.extend(base)
	pic2(message, tags)

@Client.on_message(filters.command("tails", "!"))
def foxboi(client, message):
	leniency = 0
	base = ['miles_prower', 'rating:safe']
	tags = ['-hyper', '-huge_breasts', '-feet', '-breasts', '-crossgender', 
	'-kissing', '-hand_holding', '-maid_uniform', '-butt', '-barefoot', 
	'-overweight', '-romantic','-argos90', '-nazi', '-crossdressing', 
	'-<3', '-muscular', '-nikraccoom','-crossover', '-ugandan_knuckles', 
	'-cursed_image', '-humanoid', '-black_sclera', '-kenket', '-leash', 
	'-alternate_form', '-wide_hips', '-male/male', '-moorsheadfalling', '-aakashi', 
	'-karate_akabon','-hale', '-bed', '-hasbro', '-cub',
	'-bridal_carry', '-doll', '-bride', '-groom', '-forced',
	'-ry-spirit', '-unknown_artist', '-humanization']
	meg = " ".join(message.text.split()[1:])
	cosa = len(tags)
	if meg=="fr":
		tags.append('amy_rose')
		limit = cosa+1
	else:
		limit = cosa
	trick = False
	if trick:
		remover(limit, cosa, tags, 36)
		tags.extend(base)
		pic2(message, tags)
	else:
		base = ['{tails_(sonic) ~ miles_prower}']
		tags.extend(base)
		pic(client, message, tags, leniency)

@Client.on_message(filters.command("kris", "!"))
def kris(client, message):
	leniency = -1
	tags = ['kris_(deltarune)']
	pic(client, message, tags, leniency)

@Client.on_message(filters.command("femboy", "!"))
def femboy(client, message):
	leniency = 0
	tags = ['1boy', 'thighhighs', '-1girl', '-2girls', 'trap', '-anal_object_insertion', '-pokemon', '-furry', '-tentacle_sex']
	pic(client, message, tags, leniency)

@Client.on_message(filters.command("ena", "!"))
def sha(client, message):
	leniency = 0
	tags = ['ena_(joel_g)'] + scarti + scartia
	pic(client, message, tags, leniency)

@Client.on_message(filters.command("furry", "!"))
def woof(client, message):
	scarti_f = [
		'-gore', '-scat','-disney', '-kirby_(series)', '-bdsm'
		'-disembodied_penis', '-disney','-protogen', '-urine', 
		'-chastity_cage', '-aggressive_retsuko', '-hyper', '-pregnant', '-child',
		'-cub', '-demon', '-dragon'
	]
	tags = scarti + scarti_f
	remover(len(tags), len(tags), tags, 39)
	pic2(message, tags)

@Client.on_message(filters.command("blessed", "!"))
def wholesome(client, message):
	leniency = -1
	tags = ['-bikini', '-licking', '-tits', '-ass', '-muscular', '-bondage']
	pic(client, message, tags, leniency)

@Client.on_message(filters.command("basso", "!"))
def jaco(client, message):
	leniency = 0
	tags = ['bass_guitar']
	pic(client, message, tags, leniency)

@Client.on_message(filters.command("blob", "!") & filters.user(331421138))
def palmino(client, message):
	leniency = 0
	tags = ['slime_(creature)']
	pic(client, message, tags, leniency)

def pdgmatch(tags, meg):
	char = ['waffle', 'alicia', 'flare', 'stare', 'panta',
	'russel', 'terria', 'cyan', 'fool', 'red',
	'chocolat', 'elh', 'beluga', 'quebec', 'flo',
	'bruno', 'opera', 'gren', 'calua', 'merveille',
	'nero', 'blanck', 'carmine', 'rose', 'malt',
	'mei', 'hanna', 'boron', 'kyle', 'socks',
	'chick', 'hack', 'sheena', 'jin', 'wappa',
	'britz', 'muscat', 'shvein', 'flam', 'doktor blutwurst']
	chartag = ['waffle_ryebread', 'alicia_pris', 'flare_pris', 'stare_pris', 'panta_(tail_concerto)',
	'russel_ryebread', 'princess_terria', 'cyan_garland', 'fool_(tail_concerto)', 'red_savarin',
	'chocolat_gelato', 'elh_melizee', 'beluga_damiens', 'quebec(solatorobo)', 'flo_financier',
	'bruno_dondurma', 'opera_kranz', 'gren_sacher', 'calua_napage', 'merveille_million',
	'nero_(solatorobo)', 'blanck_(solatorobo)', 'carmine_(solatorobo)', 'rose_(solatorobo)', 'malt_marzipan',
	'mei_marzipan', 'hanna_fondant', 'boron_brioche', 'kyle_bavarois', 'socks_million',
	'chick_montblanc', 'hack_montblanc', 'sheena_falafel', 'jin_macchiato', 'wappa_charlotte',
	'britz_strudel', 'muscat_(fuga)', 'shvein_hax', 'flam_kish', 'doktor_blutwurst']
	if meg != "":
		for x in range(len(char)):
			match = re.search(char[x], meg, flags=re.IGNORECASE)
			if match is not None:
				tags.append(chartag[x])
				break
			else:
				tags.append(random.choice(tuple(chartag)))
	else:
		tags.append(random.choice(tuple(chartag)))
	return tags

@Client.on_message(filters.command("char", "!"))
def pdg(client, message):
	tags = ['little_tail_bronx', 'rating:safe']
	meg = " ".join(message.text.split()[1:])
	match_set = ['s', 'f', 't']
	tag_set = ['solatorobo', 'fuga:_melodies_of_steel', 'tail_concerto']
	if meg != "":
		for x in range(len(match_set)):
			match = re.search(match_set[x], meg, flags=re.IGNORECASE)
			if match is not None:				
				tags.append(tag_set[x])
				break
			else:
				tags = [random.choice(tuple(tag_set))]
	else:
		tags = [random.choice(tuple(tag_set))]
	pdgmatch(tags, meg)
	pic2(message, tags)

@Client.on_message(filters.command("calc", "!"))
def calc(client, message):
	gesu = " ".join(message.text.split()[1:])
	message.reply_text("<code>" + str(simple_eval(gesu)) + "</code>")

@Client.on_message(filters.command("no", "!"))
def no(client, message):
	pareo = [
		'puttane', 'bitches', 'figura materna',
		'figura paterna', 'women', 'ladies',
		'wenches', 'damsels', 'tette', 'poppe',
		'badonkadonks', 'zoccole', 'palle', 'zinne',
		'minne', 'chiappette', 'goth gf', 'femboy',
		'estrogeni', 'mizore'
	]
	message.reply_text("<code>no " + random.choice(pareo) + "?</code>")

@Client.on_message(filters.command("sto", "!"))
def stocazzo(client, message):
	message.reply_text("<code>cazzo</code>")

@Client.on_message(filters.command("mela", "!"))
def mela(client, message):
	message.reply_text("<code>suchi</code>")

@Client.on_message((filters.user(492326133) | filters.user(206010198)) & filters.command("horny", "!"))
def horny(client, message):
	switchino(message)

@Client.on_message(filters.command("status", "!"))
def status(client, message):
	if consfw == True:
		h_stat = "Horny attivo"
	else:
		h_stat = "Horny non attivo"
	message.reply_text("<code>" + h_stat + "</code>")

@Client.on_message(filters.command("misura", "!"))
def pisnelo(client, message):
	boh = ""	
	ah = randint(0, 30)
	for i in range(30):
		if i == ah:
			break
		else:
			boh += ":"
	message.reply_text("<code>Ce l'hai lungo " + str(ah) + " cm\n8" + boh + "D</code>")
	

@Client.on_message(filters.command("misure", "!"))
def popipopi(client, message):
	boh = ""
	momentino = [
		"Sei più piatta della Pianura Padana", "Hai una prima, belle tettine",
		"Hai una seconda, belle poppe", "Hai una terza, jamm bell",
		"Hai una quarta, dei furgoncioni",
		"Hai una quinta, delle mozzarellone, zizzone di Battipaglia",
		"E la madonna c'hai dei meloni sul petto, hai una sesta",
		"Hai una settima, due bombe in pratica",
		"E la madonna e che sei la Parmalat? Hai un'ottava"
	]
	ah = randint(0, 9)
	for i in range(9):
		if i == ah:
			break
		else:
			boh += " "
	message.reply_text("(" + boh + "." + boh + ")(" + boh + "." + boh +")\n<code>" + momentino[ah] + "</code>")

@Client.on_message(filters.command("porco", "!"))
def diocan(client, message):
	nome = [
		'dio', 'gesù', 'giuseppe',
		'giuda', 'cristo',
		'signore', 'allah', 'buddha'
	]
	bestemmiun = [
		'interrato', 'impalato', 'sgarrupato',
		'stuprato', 'gesuita',
		'incarcerato', 'inculato', ""
	]
	eun = len(nome)
	eddoje = len(bestemmiun)
	randr = randint(0, eun)
	randc = randint(0, eddoje)
	maronn = "<code>porco " + nome[randr] + " " + bestemmiun[randc] + "</code>"
	message.reply_text(maronn)

@Client.on_message(filters.command("poppe", "!"))
def tettazze(client, message):
	message.reply_text("<code>ciao puttana, belle TETTE hahahahahha latte latttte latte baby ho tanta sete mamma hahahahaha stupida puttana dammi quelle mammellone zoccola haha tette tetta tettazze dammele io dare morsone alle tue tettone hahah popi popi popi sesso troia mammina popi popi latte baby ne voglio di più succhia succhia popi popi toc toc su quelle belle tettone lattose heee hee heeee haha aaaaaaaaa nessuno fermerà il furgoncino del latte haha poooo poooo ciuf ciuf tutti a bordo sul treno delle tettoneeee eeee wooooo poooo pooooo</code>")

@Client.on_message(filters.command("cerca", "!"))
def cerca(client, message):
	meg = " ".join(message.text.split()[1:])
	message.reply_text("t.me/addstickers/" + meg)
	
@Client.on_message(filters.command("ext", "!"))
def pinnino(client, message):
	if message.reply_to_message.text != "":
		pinurl = message.reply_to_message.text
	else:
		pinurl = " ".join(message.text.split()[1:])
	pinreq = requests.get(pinurl)
	pinres = re.search("https://i\\.pinimg\\.com.*(jpg|gif|png)\"\\s", pinreq.text, flags=re.IGNORECASE)
	if pinres is not None:
		link_temp = pinres.group(0)
		pinlink = re.sub("\".*$", "", link_temp)
	else: 
		pinlink = "API timeout"
	message.reply_text(pinlink)

@Client.on_message(filters.command("fix", "!"))
def fixino(client, message):
	if message.reply_to_message.text != "":
		link = message.reply_to_message.text
	else:
		link = " ".join(message.text.split()[1:])
	twres = re.search("twitter", link, flags=re.IGNORECASE)
	inres = re.search("instagram", link, flags=re.IGNORECASE)
	if twres is not None:
		re.sub("twitter", "fxtwitter", link)
	elif inres is not None:
		re.sub("instagram", "ddinstagram", link)
	else:
		link = "Invalid link"
	message.reply_text(link)
